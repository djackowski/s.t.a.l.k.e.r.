package com.blstream.stalker.model;

import android.support.annotation.NonNull;

/**
 * Used for strogin open hours of each place
 */
public class OpenHours {

    private String timeOpened;
    private String timeClosed;


    /**
     * Constructor that sets hours to specified
     *
     * @param timeOpened time open to be set
     * @param timeClosed time closed to be set
     */
    public OpenHours(@NonNull String timeOpened, @NonNull String timeClosed) {
        this.timeClosed = timeClosed;
        this.timeOpened = timeOpened;

    }

    /**
     * Fills array with no info
     *
     * @param openHours array to filled
     * @return filled array
     */
    public static OpenHours[] fillWithNoInfo(OpenHours[] openHours) {

        for (int i = 0; i < openHours.length; i++) {
            openHours[i] = new OpenHours("no info", "no info");
        }
        return openHours;
    }

    /**
     * Sets timeOpened with specified param
     *
     * @param timeOpened to be set
     */
    public void setTimeOpened(String timeOpened) {
        this.timeOpened = timeOpened;
    }

    /**
     * Sets timeClosed with specified param
     *
     * @param timeClosed to be set
     */
    public void setTimeClosed(String timeClosed) {
        this.timeClosed = timeClosed;
    }

    /**
     * Gets openTime
     *
     * @return time when open
     */
    public String getOpenTime() {
        return timeOpened;
    }

    /**
     * Gets closeTime
     *
     * @return time when closed
     */
    public String getCloseTime() {
        return timeClosed;
    }

    /**
     * Converts into string
     *
     * @return string with basic object's data
     */
    @Override
    public String toString() {
        return "OpenHours{" +
                "timeOpened='" + timeOpened + '\'' +
                ", timeClosed='" + timeClosed + '\'' +
                '}';
    }
}
