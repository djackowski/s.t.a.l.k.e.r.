package com.blstream.stalker;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;

import com.blstream.stalker.controller.location.LocationService;

/**
 * Sends broadcast informing that app goes to BG when onStop is called
 */
public class MainActivity extends BaseActivity {

    /**
     * {@inheritDoc}
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected void onStop() {
        super.onStop();
        sendAppInBgBroadcast();
    }

    private void sendAppInBgBroadcast() {
        Intent intent = new Intent();
        intent.setAction(LocationService.ACTION_APP_IN_BG);
        sendBroadcast(intent);
    }
}
