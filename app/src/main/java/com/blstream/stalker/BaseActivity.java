package com.blstream.stalker;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

import com.blstream.stalker.controller.GoogleApiCallbacksHandler;
import com.blstream.stalker.controller.location.LocationService;
import com.blstream.stalker.view.fragments.LoginScreenView;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.drive.Drive;
import com.google.android.gms.plus.Plus;


public abstract class BaseActivity extends AppCompatActivity {
    private static final String FRAGMENT_KEY = "LoginScreenView";
    protected GoogleApiClient googleApiClient;
    LoginScreenView loginScreenView;
    private GoogleApiCallbacksHandler callbacksHandler;

    /**
     * @return GoogleApiClient
     */
    public GoogleApiClient getGoogleApiClient() {
        return googleApiClient;
    }

    /**
     * @return GoogleApiCallbacksHandler
     */
    public GoogleApiCallbacksHandler getCallbacksHandler() {
        return callbacksHandler;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Intent intent = new Intent(this, LocationService.class);
        startService(intent);
        initializationOfSaveInstanceState(savedInstanceState);
        initializeGPApiClient();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        callbacksHandler.notifyAboutActivityResult(requestCode, resultCode, data, RESULT_OK);
        super.onActivityResult(requestCode, resultCode, data);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        getSupportFragmentManager().putFragment(outState, FRAGMENT_KEY, loginScreenView);
    }

    /**
     * Initializes google plus api client
     */
    private void initializeGPApiClient() {
        callbacksHandler = new GoogleApiCallbacksHandler();
        if (googleApiClient == null) {
            googleApiClient = new GoogleApiClient.Builder(this)
                    .addConnectionCallbacks(callbacksHandler)
                    .addOnConnectionFailedListener(callbacksHandler)
                    .addApi(Plus.API)
                    .addApi(Drive.API)
                    .addScope(Plus.SCOPE_PLUS_LOGIN)
                    .addScope(Plus.SCOPE_PLUS_PROFILE)
                    .addScope(Drive.SCOPE_FILE)
                    .build();
        }
    }

    private void initializationOfSaveInstanceState(Bundle savedInstanceState) {
        if (savedInstanceState == null) {
            loginScreenView = new LoginScreenView();
            getSupportFragmentManager().beginTransaction().add(R.id.mainContainer, loginScreenView).commit();
        } else {
            loginScreenView = (LoginScreenView) getSupportFragmentManager().getFragment(savedInstanceState, FRAGMENT_KEY);
        }
    }
}
