package com.blstream.stalker.controller;

import android.content.Context;
import android.widget.ImageView;

import com.android.volley.toolbox.ImageLoader;
import com.blstream.stalker.controller.volley.VolleySingleton;

/**
 * Manages image downloading using Volley
 */
public class ImageController {

    Context context;
    private int placeHolder;
    private int errorImage;

    /**
     * Creates Image controller that downloads image using Volley
     *
     * @param context     app context
     * @param placeHolder will be set in ImageView until corrent image is downloaded
     * @param errorImage  will be set in ImageView when error occurs during downloading
     */
    public ImageController(Context context, int placeHolder, int errorImage) {
        this.context = context;
        this.placeHolder = placeHolder;
        this.errorImage = errorImage;
    }

    /**
     * Sets Image to ImageView specified in parameter
     *
     * @param url   to image that will be downloaded
     * @param image ImageView where image will be set
     */
    public void getImage(String url, ImageView image) {
        ImageLoader loader;
        loader = VolleySingleton.getInstance(context).getImageLoader();
        loader.get(url, ImageLoader.getImageListener(image, placeHolder, errorImage));
    }
}
