package com.blstream.stalker.controller.location.sync;

import android.accounts.Account;
import android.content.AbstractThreadedSyncAdapter;
import android.content.ContentProviderClient;
import android.content.Context;
import android.content.SyncResult;
import android.os.Bundle;
import android.util.Log;

import com.blstream.stalker.controller.DatabaseController;
import com.blstream.stalker.controller.location.Constants;
import com.blstream.stalker.controller.places.GooglePlacesController;
import com.blstream.stalker.model.PlaceDataWithDetails;

import java.util.ArrayList;
import java.util.List;

/**
 * Finds places from current location and adds them to DB
 */
public class PlacesSyncAdapter extends AbstractThreadedSyncAdapter {
    private static final String TAG = "PlacesSyncAdapter: ";
    private GooglePlacesController googlePlacesController;
    private DatabaseController databaseController;

    /**
     * {@inheritDoc}
     */
    public PlacesSyncAdapter(Context context, boolean autoInitialize) {
        super(context, autoInitialize);
        googlePlacesController = new GooglePlacesController();
        databaseController = new DatabaseController(context);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void onPerformSync(Account account, Bundle extras, String authority,
                              ContentProviderClient provider, SyncResult syncResult) {
        Log.d(TAG, "onPerformSync: ");
        double longitude = extras.getDouble(Constants.LONGITUDE_KEY);
        double latitude = extras.getDouble(Constants.LATITUDE_KEY);
        List<PlaceDataWithDetails> placeDataWithDetailsList = googlePlacesController.
                findPlacesWithDetails(latitude, longitude);
        databaseController.addPlacesToDB(placeDataWithDetailsList);
        Log.d(TAG, "findplaces: DONE ");
    }
}