package com.blstream.stalker.controller;

import android.content.Intent;
import android.content.IntentSender;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.widget.Toast;

import com.blstream.stalker.Constants;
import com.blstream.stalker.controller.interfaces.IGoogleApiCallback;
import com.blstream.stalker.view.fragments.PlaceListView;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.drive.Drive;
import com.google.android.gms.drive.DriveApi;
import com.google.android.gms.drive.MetadataChangeSet;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.OutputStream;

/**
 * Handles GoogleDrive operations
 */
public class GoogleDriveController extends FragmentController<PlaceListView> implements IGoogleApiCallback {

    private static final String TAG = GoogleDriveController.class.getName();
    private GoogleApiClient googleApiClient;
    private GoogleApiCallbacksHandler googleApiCallbacksHandler;

    /**
     * @param fragment                  Fragment fragment to assign to controller
     * @param googleApiClient           GoogleApiClient handle for GoogleApiClient
     * @param googleApiCallbacksHandler GoogleApiCallbacksHandler callback handler for registering listener in camera controller
     */
    public GoogleDriveController(Fragment fragment, GoogleApiClient googleApiClient, GoogleApiCallbacksHandler googleApiCallbacksHandler) {
        super(fragment);
        this.googleApiCallbacksHandler = googleApiCallbacksHandler;
        this.googleApiClient = googleApiClient;
    }

    /**
     * Uploads taken pictures to GoogleDrive
     *
     * @param bitmapToSave Bitmap bitmap to upload on Google Drive
     * @param fileName name with which file will safe on Google Drive
     */
    public void saveFileToDrive(final Bitmap bitmapToSave, final String fileName) {
        // Start by creating a new contents, and setting a callback.
        Drive.DriveApi.newDriveContents(googleApiClient)
                .setResultCallback(new ResultCallback<DriveApi.DriveContentsResult>() {

                    /**
                     * {@inheritDoc}
                     */
                    @Override
                    public void onResult(@NonNull DriveApi.DriveContentsResult result) {
                        // If the operation was not successful, we cannot do anything
                        // and must fail.
                        if (!result.getStatus().isSuccess()) {
                            Toast.makeText(fragment.getContext(), "Unable to write file contents.", Toast.LENGTH_SHORT).show();
                            return;
                        }
                        // Get an output stream for the contents.
                        OutputStream outputStream = result.getDriveContents().getOutputStream();
                        // Write the bitmap data from it.
                        ByteArrayOutputStream bitmapStream = new ByteArrayOutputStream();
                        bitmapToSave.compress(Bitmap.CompressFormat.PNG, 100, bitmapStream);
                        try {
                            outputStream.write(bitmapStream.toByteArray());
                        } catch (IOException e1) {
                            Toast.makeText(fragment.getContext(), "Unable to write file contents.", Toast.LENGTH_SHORT).show();
                        }
                        // Create the initial metadata - MIME type and title.
                        // Note that the user will be able to change the title later.
                        MetadataChangeSet metadataChangeSet = new MetadataChangeSet.Builder()
                                .setMimeType(Constants.MIME_TYPE).setTitle(fileName).build();
                        // Create an intent for the file chooser, and start it.
                        IntentSender intentSender = Drive.DriveApi
                                .newCreateFileActivityBuilder()
                                .setInitialMetadata(metadataChangeSet)
                                .setInitialDriveContents(result.getDriveContents())
                                .build(googleApiClient);
                        try {
                            fragment.getActivity().startIntentSenderForResult(
                                    intentSender, Constants.REQUEST_CODE_CREATOR, null, 0, 0, 0);
                        } catch (IntentSender.SendIntentException e) {
                            Toast.makeText(fragment.getContext(), "Failed to launch file chooser.", Toast.LENGTH_SHORT).show();
                        }
                    }
                });
    }

    /**
     * Registers listener in GoogleDriveController
     */
    public void registerCallbackHandler() {
        googleApiCallbacksHandler.registerListener(this);
    }

    /**
     * Unregisters listener from GoogleDriveController
     */
    public void unregisterCallbackHandler() {
        googleApiCallbacksHandler.unregisterListener(this);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void onConnectionFailed(ConnectionResult connectionResult) {
        Log.d(TAG, "connectionFailedHandling: " + connectionResult);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void onConnected(Bundle bundle) {
        Log.d(TAG, "onConnected");
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void onConnectionSuspended(int i) {
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void onGoogleApiActivityResult(int requestCode, int responseCode, Intent data, int RESULT_OK) {
        Log.d(TAG, "onGoogleApiActivityResult: " + requestCode + " " + responseCode + " " + RESULT_OK);
        if (requestCode == Constants.REQUEST_CODE_CREATOR && responseCode == RESULT_OK) {
            Toast.makeText(fragment.getContext(), "Image uploaded.", Toast.LENGTH_SHORT).show();
        }
    }
}
