package com.blstream.stalker.controller;




import android.support.v4.app.Fragment;

import com.blstream.stalker.controller.FragmentController;
import com.blstream.stalker.view.fragments.DetailItemView;

/**
 *  Handles events on DetailItemView.
 */
public class DetailItemController extends FragmentController<DetailItemView> {
    /**
     *
     * {@inheritDoc}
     */
    public DetailItemController(Fragment fragment) {
        super(fragment);
    }
}
