package com.blstream.stalker.controller.location.urlconnection;

import android.net.Uri;
import android.support.annotation.NonNull;

import com.blstream.stalker.controller.location.Constants;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLConnection;

/**
 * Connects with specified url
 * Makes url with specified data
 */
public class UrlConnection {

    private static final int INPUT_STREAM_READER_SIZE = 8;
    private static final String URL_PART_LOCATION = "&location=";
    private static final String URL_PART_RADIUS = "&radius=";
    private static final String SENSOR_VALUE = "false";
    private static final String URL_PART_SENSOR = "&sensor=";
    private static final String COMA = ",";
    private static final String URL_PART_SEARCH = "search/json?";
    private static final String URL_PART_DETAILS = "details/json?";
    private static final String URL_PART_PLACEID = "placeid=";

    /**
     * Makes url with specified params for places
     *
     * @param latitude  based on user's current location
     * @param longitude based on user's current location
     * @param radius    range of searching location from user's current position
     * @return url that contains params above
     */
    @NonNull
    public String makeUrlSearchPlace(double latitude, double longitude, int radius) {
        return Constants.GOOGLE_PLACE_URL
                + URL_PART_SEARCH
                + URL_PART_LOCATION
                + Double.toString(latitude)
                + COMA
                + Double.toString(longitude)
                + URL_PART_RADIUS
                + radius
                + URL_PART_SENSOR + SENSOR_VALUE
                + Constants.API_KEY_URL_PART;
    }

    /**
     * Makes url with specified param for details of place
     *
     * @param place_id specify place to be found
     * @return url that contains params above
     */
    public String makeUrlDetailsPlace(String place_id) {
        return Uri.parse(Constants.GOOGLE_PLACE_URL
                + URL_PART_DETAILS
                + URL_PART_PLACEID
                + place_id
                + Constants.API_KEY_URL_PART).toString();
    }

    /**
     * Makes connection with specify url and gets its content
     *
     * @param theUrl to be connected
     * @return content of site specified with the url
     */
    public String getUrlContents(String theUrl) {
        StringBuilder content = new StringBuilder();
        try {
            URL url = new URL(theUrl);
            URLConnection urlConnection = url.openConnection();
            BufferedReader bufferedReader = new BufferedReader(
                    new InputStreamReader(urlConnection.getInputStream()), INPUT_STREAM_READER_SIZE);
            String line;
            while ((line = bufferedReader.readLine()) != null) {
                content.append(line).append("\n");
            }
            bufferedReader.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return content.toString();
    }


}
