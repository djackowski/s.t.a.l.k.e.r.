package com.blstream.stalker.controller.location;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v4.app.NotificationCompat;

import com.blstream.stalker.MainActivity;
import com.blstream.stalker.R;

/**
 * Receivers used to Show notification when app goes to background, and Stop service when user
 * dismisses notification from notification drawer.
 */
public class ServiceBroadcastReceiver extends BroadcastReceiver {

    /**
     * {@inheritDoc}
     */
    @Override
    public void onReceive(Context context, Intent intent) {
        switch (intent.getAction()) {
            case LocationService.ACTION_NOTIFICATION_DELETE:
                Intent intentStop = new Intent(context, LocationService.class);
                context.stopService(intentStop);
                context.unregisterReceiver(this);
                break;
            case LocationService.ACTION_APP_IN_BG:
                sendNotification(context, context.getString(R.string.notificationMessage));
                break;
        }

    }

    /**
     * Sends System notification with given message
     *
     * @param context app context
     * @param message to be set in notification
     */
    protected void sendNotification(Context context, String message) {
        NotificationManager notificationManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
        notificationManager.notify(0, getNotification(context, message));
    }

    @NonNull
    private Notification getNotification(Context context, String message) {
        Intent notificationIntent = new Intent(context, MainActivity.class);
        Intent deleteIntent = new Intent(LocationService.ACTION_NOTIFICATION_DELETE);
        //this is called when user deletes notification.
        PendingIntent deletePendingIntent = PendingIntent.getBroadcast(context, 0, deleteIntent, 0);
        //this is called when user clicks notification
        PendingIntent clickPendingIntent = PendingIntent.getActivity(context, 0, notificationIntent, 0);
        NotificationCompat.Builder notificationBuilder;
        notificationBuilder = new NotificationCompat.Builder(context)
                .setSmallIcon(R.mipmap.ic_launcher)
                .setContentTitle(context.getString(R.string.app_name))
                .setContentText(message)
                .setDeleteIntent(deletePendingIntent)
                .setDefaults(Notification.DEFAULT_SOUND | Notification.FLAG_SHOW_LIGHTS);
        notificationBuilder.setContentIntent(clickPendingIntent);
        return notificationBuilder.build();
    }

}
