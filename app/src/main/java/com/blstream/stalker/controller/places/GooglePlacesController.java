package com.blstream.stalker.controller.places;

import com.blstream.stalker.controller.location.parser.JSONParser;
import com.blstream.stalker.controller.location.urlconnection.UrlConnection;
import com.blstream.stalker.model.PlaceData;
import com.blstream.stalker.model.PlaceDataDetails;
import com.blstream.stalker.model.PlaceDataWithDetails;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

/**
 * Manages parsing and finding places based on current user's location
 */
public class GooglePlacesController {

    private static final String RESULT_KEY = "result";
    private static final String RESULTS_KEY = "results";
    private static final int RADIUS = 500;
    private UrlConnection urlConnection = new UrlConnection();
    private JSONParser jsonParser = new JSONParser();


    /**
     * Finds nearby places (based on current location) with their details
     *
     * @param latitude  based on user's current position
     * @param longitude based on user's current position
     * @return list of PlaceDataWithDetails objects
     */
    public List<PlaceDataWithDetails> findPlacesWithDetails(double latitude, double longitude) {

        String urlPlaceString = urlConnection.makeUrlSearchPlace(latitude, longitude, RADIUS);

        try {
            String json = jsonParser.getJSON(urlPlaceString);

            JSONObject object = new JSONObject(json);
            JSONArray array = object.getJSONArray(RESULTS_KEY);

            List<PlaceDataWithDetails> arrayList = new ArrayList<>();
            for (int i = 0; i < array.length(); i++) {
                try {
                    PlaceData place = jsonParser.parsePlaceDataObjects((JSONObject) array.get(i));
                    if (place != null) {
                        PlaceDataDetails placeDataDetails = findPlaceDetails(place.getPlace_id());
                        arrayList.add(new PlaceDataWithDetails(place, placeDataDetails));
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
            return arrayList;
        } catch (JSONException ex) {
            ex.printStackTrace();
        }
        return null;
    }

    /**
     * Gets place's details specified with place_id
     *
     * @param place_id id of place to be find and parsed
     * @return PlaceDataDetails object
     * @throws JSONException if specify object does not exist
     */
    public PlaceDataDetails findPlaceDetails(String place_id) throws JSONException {
        String urlString = urlConnection.makeUrlDetailsPlace(place_id);
        String json = jsonParser.getJSON(urlString);
        JSONObject object = new JSONObject(json);
        JSONObject result = (JSONObject) object.get(RESULT_KEY);
        return jsonParser.parsePlaceDataDetailsObject(result);
    }
}
