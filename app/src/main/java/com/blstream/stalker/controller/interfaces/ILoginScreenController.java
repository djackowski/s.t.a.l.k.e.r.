package com.blstream.stalker.controller.interfaces;

import android.support.v4.app.FragmentManager;

public interface ILoginScreenController {

    /**
     * Method signs in user with google plus account.
     * Called after click on button "Login" from LoginScreenView.
     */
    void googlePlusLogin();

    /**
     * Method changes fragment to fragment with lists of places.
     * Called when button "No Thanks" clicked from LoginScreenView.
     */
    void runWithoutLogin(FragmentManager manager);

    /**
     * Method handles the results from onGoogleApiActivityResult callback.
     *
     * @param requestCode  int
     * @param responseCode int
     * @param RESULT_OK    int
     */
}
