package com.blstream.stalker.controller.interfaces;

import android.content.Intent;
import android.os.Bundle;

import com.google.android.gms.common.ConnectionResult;

/**
 * Handles single callback from Google Api
 */
public interface IGoogleApiCallback {

    /**
     * Called when connection to google api failed
     *
     * @param connectionResult ConnectionResult
     */
    void onConnectionFailed(ConnectionResult connectionResult);

    /**
     * Called when connection to google api was successful
     *
     * @param bundle Bundle
     */
    void onConnected(Bundle bundle);


    /**
     * Called when connection to google api was suspended
     *
     * @param i int
     */
    void onConnectionSuspended(int i);

    /**
     * Gets result when startActivityForResult() was called
     *
     * @param requestCode  int listener code
     * @param responseCode int returned result
     * @param data         Intent returned data
     * @param RESULT_OK    int successful result pattern
     */
    void onGoogleApiActivityResult(int requestCode, int responseCode, Intent data, final int RESULT_OK);
}
