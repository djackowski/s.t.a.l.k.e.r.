package com.blstream.stalker.controller.location;

import android.app.PendingIntent;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.ActivityRecognition;

/**
 * Controller for Google Api activity Detector.
 * Detects avtivity types, when succesfully connected.
 */
public class DetectActivityController extends DetectFragmentController implements
        GoogleApiClient.OnConnectionFailedListener,
        GoogleApiClient.ConnectionCallbacks, ResultCallback<Status> {

    private static final String TAG = "DetectController: ";
    private static final int DETECTION_TIME = 0;
    private ActivityDetectionBroadcastReceiver activityDetectionBroadcastReceiver;
    private GoogleApiClient googleApiClientActivityDetector;

    /**
     * Creates Controller with fragmen given in parameter
     *
     * @param fragment fragment is used to get Context in thiss clas
     */
    public DetectActivityController(Fragment fragment) {
        super(fragment);
        activityDetectionBroadcastReceiver = new ActivityDetectionBroadcastReceiver();
        createGoogleApiClientInstance();
    }

    /**
     * {@inheritDoc}
     * called when succesfully connected to google Api Client.
     * Starts getting updates.
     *
     * @param bundle not used
     */
    @Override
    public void onConnected(@Nullable Bundle bundle) {
        Log.d(TAG, "onConnected");
        startActivityUpdates();
    }

    /**
     * {@inheritDoc}
     * Connects to ActivityDetector
     */
    @Override
    public void onConnectionSuspended(int i) {
        Log.d(TAG, "Connection suspended");
        googleApiClientActivityDetector.connect();

    }

    /**
     * {@inheritDoc}
     *
     * @param connectionResult hold information about error
     *                         prints error code in catlog
     */
    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
        Log.d(TAG, "Connection failed. Error: " + connectionResult.getErrorCode());
    }

    /**
     * {@inheritDoc}
     *
     * @param status information about result
     */
    @Override
    public void onResult(@NonNull Status status) {
        if (status.isSuccess()) {
            Log.d(TAG, "Successfully added activity detection.");
        } else {
            Log.d(TAG, "Error: " + status.getStatusMessage());
        }
    }

    /**
     * Invoke onStop in View
     */
    public void onStop() {
        if (googleApiClientActivityDetector.isConnected()) {
            googleApiClientActivityDetector.disconnect();
        }
    }

    /**
     * Invoke onStart in View
     */
    public void onStart() {
        googleApiClientActivityDetector.connect();
    }

    /**
     * Invoke onPause in View
     */
    public void onPause() {
        unregisterReceiver();
        stopActivityUpdates();
    }

    /**
     * Invoke onResume in View
     */
    public void onResume() {
        registerReceiver();
    }

    private void createGoogleApiClientInstance() {
        if (googleApiClientActivityDetector == null) {
            googleApiClientActivityDetector = new GoogleApiClient.Builder(fragment.getContext())
                    .addConnectionCallbacks(this)
                    .addOnConnectionFailedListener(this)
                    .addApi(ActivityRecognition.API)
                    .build();
        }
    }

    private PendingIntent getActivityDetectionPendingIntent() {
        Intent intent = new Intent(fragment.getContext(), DetectActivitiesIntentService.class);
        return PendingIntent.getService(fragment.getContext(), 0,
                intent, PendingIntent.FLAG_UPDATE_CURRENT);
    }

    private void unregisterReceiver() {
        LocalBroadcastManager.getInstance(fragment.getContext())
                .unregisterReceiver(activityDetectionBroadcastReceiver);
    }

    private void registerReceiver() {
        LocalBroadcastManager.getInstance(fragment.getContext())
                .registerReceiver(activityDetectionBroadcastReceiver,
                        new IntentFilter(Constants.STRING_ACTION));
    }

    private void stopActivityUpdates() {
        ActivityRecognition.ActivityRecognitionApi
                .removeActivityUpdates(googleApiClientActivityDetector,
                        getActivityDetectionPendingIntent()).setResultCallback(this);
    }

    private void startActivityUpdates() {
        ActivityRecognition.ActivityRecognitionApi
                .requestActivityUpdates(googleApiClientActivityDetector, DETECTION_TIME,
                        getActivityDetectionPendingIntent()).setResultCallback(this);
    }
}
