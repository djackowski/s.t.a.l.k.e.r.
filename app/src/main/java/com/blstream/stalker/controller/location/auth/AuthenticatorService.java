package com.blstream.stalker.controller.location.auth;

import android.app.Service;
import android.content.Intent;
import android.os.IBinder;
import android.util.Log;


/**
 * Service used to bind authenticator
 */
public class AuthenticatorService extends Service {
    private static final String TAG = "AuthenticatorService";
    private Authenticator authenticator;

    @Override
    public void onCreate() {
        Log.d(TAG, "onCreate: Service created ");
        authenticator = new Authenticator(this);
    }

    /*
    * When the system binds to this Service to make the RPC call
    * return the authenticator’s IBinder.
    */
    @Override
    public IBinder onBind(Intent intent) {
        return authenticator.getIBinder();
    }
}
