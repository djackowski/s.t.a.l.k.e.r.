package com.blstream.stalker.controller;

import android.content.Intent;
import android.content.IntentSender;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.support.v4.app.FragmentManager;
import android.widget.Toast;

import com.blstream.stalker.Constants;
import com.blstream.stalker.controller.interfaces.IGoogleApiCallback;
import com.blstream.stalker.controller.interfaces.ILoginScreenController;
import com.blstream.stalker.view.fragments.LoginScreenView;
import com.blstream.stalker.view.interfaces.IBasicView;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GoogleApiAvailability;
import com.google.android.gms.common.api.GoogleApiClient;

/**
 * Handles login operation
 */
public class LoginController extends FragmentController<LoginScreenView> implements ILoginScreenController, IGoogleApiCallback {

    private static final String TAG = LoginController.class.getName();
    private GoogleApiClient googleApiClient;
    private GoogleApiCallbacksHandler googleApiCallbacksHandler;

    /**
     * @param fragment                  instance of fragment
     * @param googleApiClient           instance of GoogleApiClient
     * @param googleApiCallbacksHandler instance of GoogleApiCallbackHandler class
     */
    public LoginController(Fragment fragment, GoogleApiClient googleApiClient, GoogleApiCallbacksHandler googleApiCallbacksHandler) {
        super(fragment);
        this.googleApiClient = googleApiClient;
        this.googleApiCallbacksHandler = googleApiCallbacksHandler;
    }

    /**
     * Called from LoginScreenView in onClick on login button
     */
    public void googlePlusLogin() {
        if (!googleApiClient.isConnecting()) {
            googleApiClient.connect();
        }
    }

    /**
     * Called from LoginScreenView in onClick on "No Thanks" button
     *
     * @param manager instance of FragmentManager
     */
    public void runWithoutLogin(FragmentManager manager) {
        view.changeFragment(manager, IBasicView.LIST_FRAGMENT);
    }

    /**
     * Registers listener in login controller
     */
    public void registerCallbackHandler() {
        googleApiCallbacksHandler.registerListener(this);
    }

    /**
     * Unregisters listener from login controller
     */
    public void unregisterCallbackHandler() {
        googleApiCallbacksHandler.unregisterListener(this);
    }

    private void connectionSuccessHandling(FragmentManager manager) {
        view.changeFragment(manager, IBasicView.LIST_FRAGMENT);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void onConnectionFailed(ConnectionResult connectionResult) {
        connectionFailedHandling(connectionResult);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void onConnected(Bundle bundle) {
        connectionSuccessHandling(fragment.getActivity().getSupportFragmentManager());
    }

    /**
     * {@inheritDoc}
     */

    @Override
    public void onConnectionSuspended(int i) {
    }

    /**
     * {@inheritDoc}
     *
     * @param requestCode  int listener code
     * @param responseCode int returned result
     * @param data         Intent returned data
     * @param RESULT_OK    int pattern successful result
     */
    @Override
    public void onGoogleApiActivityResult(int requestCode, int responseCode, Intent data, int RESULT_OK) {
        Log.d(TAG, "onGoogleApiActivityResult: " + responseCode + " " + RESULT_OK);
        sendLoginResultToController(requestCode, responseCode, RESULT_OK);
    }

    private void sendLoginResultToController(int requestCode, int responseCode, int RESULT_OKEY) {
        if (requestCode == Constants.RC_SIGN_IN && !googleApiClient.isConnecting() && responseCode == RESULT_OKEY) {
            googleApiClient.connect();
        }
    }

    private void connectionFailedHandling(ConnectionResult connectionResult) {
        Log.d(TAG, "connectionFailedHandling: " + connectionResult);
        if (connectionResult != null && !connectionResult.hasResolution()) {
            GoogleApiAvailability googleApiAvailability = GoogleApiAvailability.getInstance();
            int code = googleApiAvailability.isGooglePlayServicesAvailable(fragment.getContext());
            if (googleApiAvailability.isUserResolvableError(code)) {
                googleApiAvailability.getErrorDialog(fragment.getActivity(), code, Constants.RC_SIGN_IN).show();
            }
        } else {
            try {
                if (connectionResult != null) {
                    connectionResult.startResolutionForResult(fragment.getActivity(), Constants.RC_SIGN_IN);
                }
            } catch (IntentSender.SendIntentException e) {
                Toast.makeText(fragment.getContext(), "SendIntent Exception. Try again.", Toast.LENGTH_SHORT).show();
            }
        }
    }
}

