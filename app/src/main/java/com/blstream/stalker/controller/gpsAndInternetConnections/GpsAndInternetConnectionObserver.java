package com.blstream.stalker.controller.gpsAndInternetConnections;

import java.util.Observable;


/**
 * class used to notify observers abaut internet and gps connection status change.
 */
public class GpsAndInternetConnectionObserver extends Observable {

    private static GpsAndInternetConnectionObserver instance = null;


    private GpsAndInternetConnectionObserver() {
    }

    /**
     * @return singleton instance of InternetConnection observer
     */
    public static GpsAndInternetConnectionObserver getInstance() {
        if (instance == null) {
            return instance = new GpsAndInternetConnectionObserver();
        } else {
            return instance;
        }

    }

    /**
     * Notifing observers when internet connection status has changed.
     */
    public void notifyMyObservers() {
        this.setChanged();
        this.notifyObservers();
    }
}
