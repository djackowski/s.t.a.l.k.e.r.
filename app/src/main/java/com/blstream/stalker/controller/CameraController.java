package com.blstream.stalker.controller;

import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.widget.Toast;

import com.blstream.stalker.Constants;
import com.blstream.stalker.controller.interfaces.IGoogleApiCallback;
import com.blstream.stalker.view.fragments.PlaceListView;
import com.google.android.gms.common.ConnectionResult;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

/**
 * Handles camera events
 */
public class CameraController extends FragmentController<PlaceListView> implements IGoogleApiCallback {

    private static final String TAG = CameraController.class.getName();
    private GoogleApiCallbacksHandler googleApiCallbacksHandler;
    private static String fileName;

    /**
     * @param fragment                  Fragment fragment to assign to controller
     * @param googleApiCallbacksHandler GoogleApiCallbacksHandler callback handler for registering listener in camera controller
     */
    public CameraController(Fragment fragment, GoogleApiCallbacksHandler googleApiCallbacksHandler) {
        super(fragment);
        this.googleApiCallbacksHandler = googleApiCallbacksHandler;
    }

    /**
     * Handles onClick on camera button
     */
    public void onCameraClick() {
        Uri fileUri = getOutputMediaFile();
        Intent cameraIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        fragment.getActivity().startActivityForResult(cameraIntent, Constants.CAPTURE_IMAGE_ACTIVITY_REQUEST_CODE);
    }

    /**
     * Sends taken picture to GoogleDriveController which uploads it to GoogleDrive
     *
     * @param requestCode  int listener code
     * @param responseCode int returned result
     * @param data         Intent returned data
     * @param RESULT_OK    int pattern successful result
     */
    public void sendCameraResultToGoogleDrive(int requestCode, int responseCode, Intent data, final int RESULT_OK) {
        if (requestCode == Constants.CAPTURE_IMAGE_ACTIVITY_REQUEST_CODE && responseCode == RESULT_OK) {
            Bundle extras = data.getExtras();
            Bitmap bitmapToSave = (Bitmap) extras.get("data");
            Toast.makeText(fragment.getContext(), "Photo taken", Toast.LENGTH_LONG).show();
            view.getGoogleDriveController().saveFileToDrive(bitmapToSave, fileName);
        }
    }

    /**
     * Registers listener in camera controller
     */
    public void registerCallbackHandler() {
        googleApiCallbacksHandler.registerListener(this);
    }

    /**
     * Unregisters listener from camera controller
     */
    public void unregisterCallbackHandler() {
        googleApiCallbacksHandler.unregisterListener(this);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void onConnectionFailed(ConnectionResult connectionResult) {
        Log.d(TAG, "onConnectionFailed: " + connectionResult);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void onConnected(Bundle bundle) {
        Log.d(TAG, "onConnected");
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void onConnectionSuspended(int i) {
    }

    /**
     * Called after a photo has been taken.
     * {@inheritDoc}
     *
     * @param requestCode  int listener code
     * @param responseCode int returned result
     * @param data         Intent returned data
     * @param RESULT_OK    int successful result pattern
     */
    @Override
    public void onGoogleApiActivityResult(int requestCode, int responseCode, Intent data, int RESULT_OK) {
        Log.d(TAG, "onGoogleApiActivityResult: " + requestCode + " " + responseCode + " " + RESULT_OK);
        sendCameraResultToGoogleDrive(requestCode, responseCode, data, RESULT_OK);
    }

    /**
     * Creates a file for saving an image
     */
    private static Uri getOutputMediaFile() {
        File mediaStorageDir = new File(Environment.getExternalStoragePublicDirectory(
                Environment.DIRECTORY_PICTURES), Constants.SAVE_FOLDER);
        if (!mediaStorageDir.exists()) {
            if (!mediaStorageDir.mkdirs()) {
                Log.d(Constants.SAVE_FOLDER, " failed to create directory");
                return null;
            }
        }
        String timeStamp = new SimpleDateFormat(Constants.DATE_FORMAT, Locale.getDefault()).format(new Date());
        fileName = Constants.FILENAME_PREFIX + timeStamp + Constants.FILENAME_EXTENSION;
        File mediaFile;
        mediaFile = new File(mediaStorageDir.getPath() + File.separator + fileName);
        return Uri.fromFile(mediaFile);
    }
}
