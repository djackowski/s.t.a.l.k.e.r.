package com.blstream.stalker.controller.location;

/**
 * Contains basic fields used in Location ang GooglePlaces things
 */
public abstract class Constants {

    public final static String PACKAGE_NAME = "com.blstream.stalker.controller.location";
    public static final String STRING_ACTION = PACKAGE_NAME + ".STRING_ACTION";
    public static final String STRING_EXTRA = PACKAGE_NAME + ".STRING_EXTRA";
    public static final String LATITUDE_KEY = "latitude";
    public static final String LONGITUDE_KEY = "longitude";
    public static final String ACCOUNT_TYPE = "blstream.stalker.com";
    public static final String API_KEY = "AIzaSyBsvTBbnekAk_vhVm9hcZR8xy3HRo4KbRo";
    public static final String GOOGLE_PLACE_URL = "https://maps.googleapis.com/maps/api/place/";
    public static final String API_KEY_URL_PART = "&key=" + Constants.API_KEY;


}
