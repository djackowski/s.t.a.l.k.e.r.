package com.blstream.stalker.controller;

import android.support.v4.app.Fragment;

import com.blstream.stalker.view.interfaces.IBasicView;


/**
 *
 * @param <T> instance of view extends on IBasicView
 */
public abstract class FragmentController<T extends IBasicView> {

    protected Fragment fragment;
    protected T view;

    /**
     *
     * @param fragment instance of Fragment;
     */
    public FragmentController(Fragment fragment) {
        this.fragment = fragment;
    }

    /**
     * @param view instance of view extending IBasicView
     */
    public void setView(T view) {
        this.view = view;
    }
}