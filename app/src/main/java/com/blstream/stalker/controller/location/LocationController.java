package com.blstream.stalker.controller.location;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.Log;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.GoogleApiClient.ConnectionCallbacks;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;

import static com.google.android.gms.common.api.GoogleApiClient.OnConnectionFailedListener;

/**
 * Manages user's current locations
 */

public class LocationController implements OnConnectionFailedListener, ConnectionCallbacks {

    private static final String TAG = "LocationController: ";
    private static final int INTERVAL_TIME = 2000000;
    private static final int FASTEST_INTERVAL = 500;
    private GoogleApiClient googleApiClientLocation;
    private LocationRequest locationRequest;
    private LocationListener listener;

    /**
     * Sets Listener, creates GoogleApi instance and Location Request
     *
     * @param listener where onLocationChanged will be called
     * @param context  for creating googleApi instance
     */
    public LocationController(LocationListener listener, Context context) {
        this.listener = listener;
        createGoogleApiClientInstance(context);
        createLocationRequest();
    }

    /**
     * Gets state of google api
     *
     * @return true -  GoogleApiClient connected, false - if not
     */
    private boolean getGoogleApiState() {
        return googleApiClientLocation.isConnected();
    }

    /**
     * Creates location request and sets its intervals and priority
     */
    private void createLocationRequest() {
        //TODO: Interval depends on activity method(running, cycling etc) somewhere else it should be
        locationRequest = new LocationRequest();
        locationRequest.setInterval(INTERVAL_TIME);
        locationRequest.setFastestInterval(FASTEST_INTERVAL);//works when location has been found earlier
        locationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        LocationSettingsRequest.Builder builder = new LocationSettingsRequest.Builder();
        builder.addLocationRequest(locationRequest);
    }

    /**
     * Connects google api client
     */
    private void connectGoogleApiClient() {
        googleApiClientLocation.connect();
    }

    /**
     * Disconnects google api client
     */
    private void disconnectGoogleApiClient() {
        if (googleApiClientLocation.isConnected()) {
            googleApiClientLocation.disconnect();
        }
    }

    /**
     * Creates google api client instance
     */
    private void createGoogleApiClientInstance(Context context) {
        if (googleApiClientLocation == null) {
            googleApiClientLocation = new GoogleApiClient.Builder(context)
                    .addConnectionCallbacks(this)
                    .addOnConnectionFailedListener(this)
                    .addApi(LocationServices.API)
                    .build();
        }
    }

    /**
     * Invoke in onDestroy() in Service
     */
    public void onDestroy() {
        if (getGoogleApiState()) {
            stopLocationUpdates();
        }
        disconnectGoogleApiClient();
    }

    /**
     * Invoke in onStart() in fragment
     */
    public void onStart() {
        connectGoogleApiClient();
        if (getGoogleApiState()) {
            startLocationUpdates();
        }
    }


    /**
     * {@inheritDoc}
     */
    @Override
    public void onConnected(@Nullable Bundle bundle) {
        startLocationUpdates();
        Log.d(TAG, "onConnected: ");
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void onConnectionSuspended(int i) {
        Log.d(TAG, "Connection Suspended");
        googleApiClientLocation.connect();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
        Log.d(TAG, "Connection failed. Error: " + connectionResult.getErrorCode());
    }

    /**
     * Starts updates of user's location
     */
    private void startLocationUpdates() {
        LocationServices.FusedLocationApi.requestLocationUpdates(
                googleApiClientLocation, locationRequest, listener);
    }

    /**
     * Stops updates of user's location
     */
    private void stopLocationUpdates() {
        LocationServices.FusedLocationApi.removeLocationUpdates(
                googleApiClientLocation, listener);
    }


}
