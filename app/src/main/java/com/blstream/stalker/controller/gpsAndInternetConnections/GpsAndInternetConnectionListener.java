package com.blstream.stalker.controller.gpsAndInternetConnections;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;


/**
 * Class listen to internet and gps connection status change;
 */
public class GpsAndInternetConnectionListener extends BroadcastReceiver {
    private GpsAndInternetConnectionObserver observer;

    public GpsAndInternetConnectionListener() {
        observer = GpsAndInternetConnectionObserver.getInstance();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void onReceive(Context context, Intent intent) {
        observer.notifyMyObservers();
    }
}
