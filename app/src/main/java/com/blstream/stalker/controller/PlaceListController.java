package com.blstream.stalker.controller;


import android.support.v4.app.Fragment;

import com.blstream.stalker.view.fragments.PlaceListView;

/**
 * Handles Place List events
 */
public class PlaceListController extends FragmentController<PlaceListView> {

    /**
     * {@inheritDoc}
     */
    public PlaceListController(Fragment fragment) {
        super(fragment);
    }
}
