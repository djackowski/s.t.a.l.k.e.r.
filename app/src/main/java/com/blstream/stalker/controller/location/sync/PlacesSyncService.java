package com.blstream.stalker.controller.location.sync;

import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.os.IBinder;
import android.support.annotation.Nullable;
import android.util.Log;

/**
 * Binds SyncAdapter
 */
public class PlacesSyncService extends Service {
    private static final String TAG = "PlacesSyncService";
    private static   PlacesSyncAdapter placesSyncAdapter = null;
    private static final Object syncAdapterLock = new Object();

    public PlacesSyncService() {
        super();
    }

    @Override
    public void onCreate() {
        super.onCreate();
        Log.d(TAG, "onCreate: ");
        synchronized (syncAdapterLock) {
            if (placesSyncAdapter == null) {
                placesSyncAdapter = new PlacesSyncAdapter(getApplicationContext(), true);
            }
        }
    }
    
    /**
     *{@inheritDoc}
     */
    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return placesSyncAdapter.getSyncAdapterBinder();
    }
}
