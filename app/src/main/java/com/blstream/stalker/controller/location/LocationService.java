package com.blstream.stalker.controller.location;

import android.accounts.Account;
import android.accounts.AccountManager;
import android.app.Service;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.location.Location;
import android.os.Bundle;
import android.os.IBinder;
import android.support.annotation.Nullable;
import android.util.Log;

import com.blstream.stalker.R;
import com.blstream.stalker.controller.database.DatabaseContract;
import com.google.android.gms.location.LocationListener;

/**
 * Starts LocationController, and on LocationChanged(Location)
 * requestsSync to Download information about surrounding places, parses it, and adds to Database
 */
public class LocationService extends Service implements LocationListener {
    public static final String ACC_NAME = "dummyaccount";
    public static final String ACTION_APP_IN_BG = "com.blstream.stalker.controller.location.app_in_bg";
    public static final String ACTION_NOTIFICATION_DELETE = "NOTIFICATION_DELETED";
    private static final String TAG = "LocationService: ";
    public static final String ACTION_LOCATION_CHANGED = "Location_Changed";
    public static final String KEY_LOCATION = "Location";
    private LocationController locationController;
    private Account defaultAccount;

    /**
     * Called by the system when the service is first created.  Do not call this method directly.
     */
    @Override
    public void onCreate() {
        super.onCreate();
        locationController = new LocationController(this, this);
        defaultAccount = createDefaultAccount(this);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        Log.d(TAG, "onStartCommand: Service started");
        locationController.onStart();
        IntentFilter filter = new IntentFilter();
        filter.addAction(ACTION_APP_IN_BG);
        filter.addAction(ACTION_NOTIFICATION_DELETE);
        ServiceBroadcastReceiver receiver = new ServiceBroadcastReceiver();
        registerReceiver(receiver, filter);
        return super.onStartCommand(intent, flags, startId);
    }


    /**
     * {@inheritDoc}
     */
    @Override
    public void onDestroy() {
        super.onDestroy();
        locationController.onDestroy();
        Log.d(TAG, "onDestroy: Service stopped");
    }

    /**
     * {@inheritDoc}
     * Returns null, clients can not bin to this Service
     */
    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    /**
     * {@inheritDoc}
     * Requests sync adapter to download and parse data about places.
     * Creates notification when app is going to bg
     */
    @Override
    public void onLocationChanged(Location location) {
        Intent intent = new Intent();
        intent.setAction(ACTION_LOCATION_CHANGED);
        intent.putExtra(KEY_LOCATION, location);
        sendBroadcast(intent);
        Log.d(TAG, "onLocationChanged: requesting Sync.");
        Bundle bundle = new Bundle();
        bundle.putBoolean(ContentResolver.SYNC_EXTRAS_EXPEDITED, true);
        bundle.putBoolean(ContentResolver.SYNC_EXTRAS_MANUAL, true);
        bundle.putDouble(Constants.LONGITUDE_KEY, location.getLongitude());
        bundle.putDouble(Constants.LATITUDE_KEY, location.getLatitude());
        ContentResolver.requestSync(defaultAccount, DatabaseContract.AUTHORITY, bundle);
    }

    private Account createDefaultAccount(Context context) {
        // Create the account type and default account
        Account newAccount = new Account(ACC_NAME, Constants.ACCOUNT_TYPE);
        AccountManager accountManager = (AccountManager) context.getSystemService(Context.ACCOUNT_SERVICE);
        // If the account already exists no harm is done but
        // a warning will be logged.
        accountManager.addAccountExplicitly(newAccount, null, null);
        return newAccount;
    }

}
