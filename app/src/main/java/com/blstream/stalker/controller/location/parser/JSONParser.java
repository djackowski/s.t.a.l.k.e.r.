package com.blstream.stalker.controller.location.parser;

import com.blstream.stalker.controller.location.Constants;
import com.blstream.stalker.controller.location.urlconnection.UrlConnection;
import com.blstream.stalker.model.OpenHours;
import com.blstream.stalker.model.PlaceData;
import com.blstream.stalker.model.PlaceDataDetails;
import com.blstream.stalker.model.PlaceLocation;
import com.blstream.stalker.model.Review;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

/**
 * Parses json objects
 */
public class JSONParser {
    private static final int MAX_NUMBER_OF_REVIEWS = 3;
    private static final String GEOMETRY_KEY = "geometry";
    private static final String LOCATION_KEY = "location";
    private static final String NAME_KEY = "name";
    private static final String LATITUDE_KEY = "lat";
    private static final String LONGITUDE_KEY = "lng";
    private static final String PLACE_ID_KEY = "place_id";
    private static final String TYPES_KEY = "types";
    private static final String DEFAULT_INFORMATION = "no info";
    private static final double DEFAULT_RATING = 0.0;
    private static final String REVIEWS_KEY = "reviews";
    private static final String AUTHOR_NAME_KEY = "author_name";
    private static final String RATING_KEY = "rating";
    private static final String TEXT_KEY = "text";
    private static final String OPENING_HOURS_KEY = "opening_hours";
    private static final String PERIODS_KEY = "periods";
    private static final String CLOSE_KEY = "close";
    private static final String TIME_KEY = "time";
    private static final String OPEN_KEY = "open";
    private static final String PHOTOS_KEY = "photos";
    private static final String PHOTO_REFERENCE_KEY = "photo_reference";
    private static final String NO_IMAGE_AVAILABLE_URL = "http://vignette1.wikia.nocookie.net/bokunoheroacademia/images/d/d5/NoPicAvailable.png/revision/latest?cb=20141109180918";
    private static final String MAX_ICON_SIZE_VALUE = "400";
    private static final String URL_PART_PHOTOREFERENCE = "&photoreference=";
    private static final String URL_PART_PHOTO = "photo?";
    private static final String URL_PART_MAX_PHOTO_SIZE = "maxwidth=";
    private static final int NUMBER_OF_DAYS = 7;

    private UrlConnection urlConnection = new UrlConnection();

    /**
     * Parses PlaceData object
     *
     * @param jsonObject to be parsed
     * @return PlaceData objects when parse went correctly otherwise null
     */
    public PlaceData parsePlaceDataObjects(JSONObject jsonObject) {
        try {
            JSONObject geometry = (JSONObject) jsonObject.get(GEOMETRY_KEY);
            JSONObject location = (JSONObject) geometry.get(LOCATION_KEY);

            String types = parseTypes(jsonObject);
            String iconReference = parseIconReference(jsonObject);
            String icon = makeIconUrl(iconReference);
            return new PlaceData(icon,
                    types,
                    jsonObject.getString(NAME_KEY),
                    new PlaceLocation((Double) location.get(LATITUDE_KEY),
                            (Double) location.get(LONGITUDE_KEY)),
                    jsonObject.getString(PLACE_ID_KEY));
        } catch (JSONException ex) {
            ex.printStackTrace();
        }
        return null;
    }

    /**
     * Gets json code from specified url
     *
     * @param url containing json code
     * @return json code
     */
    public String getJSON(String url) {
        return urlConnection.getUrlContents(url);
    }

    /**
     * Parses PlaceDataDetails object from specified param
     *
     * @param jsonObject to be parsed
     * @return PlaceDataDetails object
     * @throws JSONException
     */
    public PlaceDataDetails parsePlaceDataDetailsObject(JSONObject jsonObject) throws JSONException {
        OpenHours[] openHours = parseEveryDayHours(jsonObject);
        double rating = parseRating(jsonObject);
        List<Review> reviewList = new ArrayList<>(parseReviewList(jsonObject, MAX_NUMBER_OF_REVIEWS));
        return new PlaceDataDetails(openHours, rating, reviewList);
    }

    /**
     * Separates elements of JSONArray with specified separator and replaces "_" to " "
     *
     * @param jsonObject to be parsed
     * @return types of PlaceData object separated with ", "
     * @throws JSONException
     */
    protected String parseTypes(JSONObject jsonObject) throws JSONException {
        JSONArray types = jsonObject.getJSONArray(TYPES_KEY);
        return types.join(", ").replaceAll("_", " ");
    }


    /**
     * Parses maxNumberOfReviews reviews from specifies jsonObject
     *
     * @param jsonObject         to be parsed
     * @param maxNumberOfReviews amount of reviews to be parsed
     * @return list of parsed reviews
     * @throws JSONException
     */
    protected List<Review> parseReviewList(JSONObject jsonObject, int maxNumberOfReviews) throws JSONException {
        List<Review> reviewList = new ArrayList<>();
        JSONArray reviewArray;
        String author = DEFAULT_INFORMATION;
        String review = DEFAULT_INFORMATION;
        double rating = DEFAULT_RATING;
        if (jsonObject.has(REVIEWS_KEY)) {
            reviewArray = jsonObject.getJSONArray(REVIEWS_KEY);
            for (int i = 0; i < reviewArray.length(); i++) {
                if (i >= maxNumberOfReviews) {
                    break;
                }

                JSONObject jsonElementOfArray = reviewArray.getJSONObject(i);
                if (jsonElementOfArray.has(AUTHOR_NAME_KEY)) {
                    author = jsonElementOfArray.getString(AUTHOR_NAME_KEY);
                }
                if (jsonElementOfArray.has(RATING_KEY)) {
                    rating = jsonElementOfArray.getDouble(RATING_KEY);
                }
                if (jsonElementOfArray.has(TEXT_KEY)) {
                    review = jsonElementOfArray.getString(TEXT_KEY);
                }

                reviewList.add(new Review(author, review, rating));

            }
        }
        return reviewList;
    }

    /**
     * Parses rating from specified jsonObject
     *
     * @param jsonObject to be parsed
     * @return value of rating if exists, otherwise default value
     * @throws JSONException
     */
    protected double parseRating(JSONObject jsonObject) throws JSONException {
        return jsonObject.has(RATING_KEY) ? jsonObject.getDouble(RATING_KEY) : DEFAULT_RATING;
    }

    /**
     * Parses everyday hours from specified jsonObject
     *
     * @param jsonObject to be parsed
     * @return array of OpenHours parsed elements
     * @throws JSONException
     */
    protected OpenHours[] parseEveryDayHours(JSONObject jsonObject) throws JSONException {
        JSONObject openingHours;
        JSONArray periods;
        OpenHours[] openHours = new OpenHours[NUMBER_OF_DAYS];
        OpenHours.fillWithNoInfo(openHours);
        if (jsonObject.has(OPENING_HOURS_KEY)) {
            openingHours = (JSONObject) jsonObject.get(OPENING_HOURS_KEY);
            if (openingHours.has(PERIODS_KEY)) {
                periods = (JSONArray) openingHours.get(PERIODS_KEY);
                searchAndParseOpenCloseTime(periods, openHours);
            }
        }
        return openHours;
    }


    /**
     * Parses icon reference from specified jsonObject
     *
     * @param jsonObject to be parsed
     * @return reference to place's icon
     * @throws JSONException
     */
    protected String parseIconReference(JSONObject jsonObject) throws JSONException {
        JSONArray photos;
        String photo_reference = null;
        if (jsonObject.has(PHOTOS_KEY)) {
            photos = jsonObject.getJSONArray(PHOTOS_KEY);
            for (int i = 0; i < photos.length(); i++) {
                JSONObject arrayElement = photos.getJSONObject(i);
                if (arrayElement.has(PHOTO_REFERENCE_KEY)) {
                    photo_reference = arrayElement.getString(PHOTO_REFERENCE_KEY);
                }
            }
        }
        return photo_reference;
    }

    /**
     * Creates url witch contains specified reference of icon
     *
     * @param reference to be added to url
     * @return string of icon url if reference not null, otherwise NO_IMAGE_AVAILABLE_URL
     */
    protected String makeIconUrl(String reference) {
        if (reference != null) {
            return Constants.GOOGLE_PLACE_URL
                    + URL_PART_PHOTO
                    + URL_PART_MAX_PHOTO_SIZE
                    + MAX_ICON_SIZE_VALUE
                    + URL_PART_PHOTOREFERENCE
                    + reference
                    + Constants.API_KEY_URL_PART;
        } else {
            return NO_IMAGE_AVAILABLE_URL;
        }
    }

    private void searchAndParseOpenCloseTime(JSONArray periods, OpenHours[] openHours) throws JSONException {
        for (int i = 0; i < periods.length(); i++) {
            JSONObject jsonElementOfPeriods = periods.getJSONObject(i);
            if (jsonElementOfPeriods.has(CLOSE_KEY)) {
                openHours[i].setTimeClosed(jsonElementOfPeriods.getJSONObject(CLOSE_KEY)
                        .getString(TIME_KEY));
            }
            if (jsonElementOfPeriods.has(OPEN_KEY)) {
                openHours[i].setTimeOpened(jsonElementOfPeriods.getJSONObject(OPEN_KEY)
                        .getString(TIME_KEY));
            }
        }
    }
}
