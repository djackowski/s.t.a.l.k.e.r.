package com.blstream.stalker;

/**
 * Contains constants
 */
public final class Constants {
    public static final int RC_SIGN_IN = 0;
    public static final int CAPTURE_IMAGE_ACTIVITY_REQUEST_CODE = 1;
    public static final int REQUEST_CODE_CREATOR = 2;
    public static final String SAVE_FOLDER = "MyCameraApp";
    public static final String DATE_FORMAT = "yyyyMMdd_HHmmss";
    public static final String FILENAME_PREFIX = "IMG_";
    public static final String FILENAME_EXTENSION = ".jpg";
    public static final String MIME_TYPE = "image/jpeg";
}
