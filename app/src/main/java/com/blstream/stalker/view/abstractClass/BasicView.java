package com.blstream.stalker.view.abstractClass;

import android.content.Context;
import android.location.LocationManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.blstream.stalker.R;
import com.blstream.stalker.controller.gpsAndInternetConnections.GpsAndInternetConnectionObserver;
import com.blstream.stalker.view.fragments.DetailItemView;
import com.blstream.stalker.view.fragments.ErrorMessageView;
import com.blstream.stalker.view.fragments.LoginScreenView;
import com.blstream.stalker.view.fragments.PlaceListView;
import com.blstream.stalker.view.interfaces.IBasicView;

import java.util.Observable;
import java.util.Observer;

/**
 * Implements methods from interface IBasicView
 */
public abstract class BasicView extends Fragment implements IBasicView, Observer {

    private static ErrorMessageView errorFragmentInstance = null;
    private static GpsAndInternetConnectionObserver observer;

    /**
     * {@inheritDoc}
     */
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return super.onCreateView(inflater, container, savedInstanceState);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        errorFragmentInstance = getErrorMessageInstance();
        observer = GpsAndInternetConnectionObserver.getInstance();
        observer.addObserver(this);
    }

    /**
     * {@inheritDoc}
     * Used to show errors on screen.
     */
    @Override
    public void update(Observable observable, Object data) {
        errorMessageSetup(getContext(), isInternetConnection((ConnectivityManager) getContext().getSystemService(Context.CONNECTIVITY_SERVICE)),
                isGpsConnection((LocationManager) getContext().getSystemService(Context.LOCATION_SERVICE)),
                getActivity().getSupportFragmentManager());
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void onPause() {
        observer.deleteObserver(this);
        super.onPause();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        update(null, null);
        super.onActivityCreated(savedInstanceState);
    }


    /**
     * {@inheritDoc}
     */
    @Override
    public void changeFragment(FragmentManager fragmentManager, @FragmentType int fragmentType) {
        switch (fragmentType) {
            case LIST_FRAGMENT:
                replaceFragment(fragmentManager, new PlaceListView());
                break;
            case DETAIL_FRAGMENT:
                replaceFragment(fragmentManager, new DetailItemView());
                break;
            case LOGIN_FRAGMENT:
                replaceFragment(fragmentManager, new LoginScreenView());
                break;
            default:
                break;
        }
    }

    /**
     * @return true if is internet connection, false if not.
     */
    public boolean isInternetConnection(ConnectivityManager connectivityManager) {
        NetworkInfo netInfo = connectivityManager.getActiveNetworkInfo();
        return netInfo != null && netInfo.isConnectedOrConnecting();
    }

    /**
     * @return true if is gps connection, false if not
     */
    public boolean isGpsConnection(LocationManager manager) {
        return manager.isProviderEnabled(LocationManager.GPS_PROVIDER);
    }

    /**
     * Setup error message in View.
     *
     * @param context              instance of context
     * @param isInternetConnection true if is internet connection, false if not
     * @param isGpsConnection      true if is gps connection, false if not
     * @param manager              instance of Fragment Manager class.
     */
    protected void errorMessageSetup(Context context, boolean isInternetConnection, boolean isGpsConnection, FragmentManager manager) {
        if (isInternetConnection & isGpsConnection) {
            hideError(manager);
        } else {
            if ((!isInternetConnection) & (!isGpsConnection)) {
                showError(errorFragmentInstance, context, manager, NO_INTERNET_AND_GPS_CONNECTION_ERROR);
            } else if (!isInternetConnection) {
                showError(errorFragmentInstance, context, manager, NO_INTERNET_CONNECTION_ERROR);
            } else {
                showError(errorFragmentInstance, context, manager, NO_GPS_CONNECTION_ERROR);
            }
        }
    }

    /**
     * @return error message view instance
     */
    protected static ErrorMessageView getErrorMessageInstance() {
        if (errorFragmentInstance == null) {
            return new ErrorMessageView();
        }
        return errorFragmentInstance;
    }

    /**
     * @param errorFragmentInstance instance of error message view class
     * @param context               context
     * @param manager               instance of Fragment Manager
     * @param errorMode             {@link #NO_INTERNET_AND_GPS_CONNECTION_ERROR},{@link #NO_GPS_CONNECTION_ERROR},{@link #NO_INTERNET_CONNECTION_ERROR}
     */
    protected void showError(ErrorMessageView errorFragmentInstance, Context context, FragmentManager manager, @ErrorMode int errorMode) {
        String msg = context.getString(errorMode);
        if (!TextUtils.isEmpty(msg)) {
            if (errorFragmentInstance.isAdded()) {
                errorFragmentInstance.changeErrorMessage(msg);
            } else {
                errorFragmentInstance.setErrorMessage(msg);
                manager.beginTransaction()
                        .setCustomAnimations(R.anim.enter, R.anim.exit, R.anim.pop_enter, R.anim.pop_exit)
                        .replace(R.id.mainErrorMessageContainer, errorFragmentInstance).commit();
            }
        }
    }


    /**
     * Hide error message.
     *
     * @param manager instance of Fragment Manager
     */
    protected void hideError(FragmentManager manager) {
        if (manager != null) {
            manager.beginTransaction()
                    .setCustomAnimations(R.anim.enter, R.anim.exit, R.anim.pop_enter, R.anim.pop_exit)
                    .remove(errorFragmentInstance).commit();
        }
    }

    /**
     * Replacing fragment using fragment manager.
     *
     * @param fragmentManager instance of fragment manager.
     * @param fragment        instance of fragment to replace.
     */
    protected void replaceFragment(FragmentManager fragmentManager, Fragment fragment) {
        fragmentManager.beginTransaction()
                .setCustomAnimations(R.anim.enter, R.anim.exit, R.anim.pop_enter, R.anim.pop_exit)
                .replace(R.id.mainContainer, fragment)
                .addToBackStack("").commit();
    }
}

