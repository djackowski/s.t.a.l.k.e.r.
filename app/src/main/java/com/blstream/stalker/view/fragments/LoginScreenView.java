package com.blstream.stalker.view.fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.blstream.stalker.BaseActivity;
import com.blstream.stalker.R;
import com.blstream.stalker.controller.GoogleApiCallbacksHandler;
import com.blstream.stalker.controller.LoginController;
import com.blstream.stalker.view.abstractClass.BasicView;
import com.blstream.stalker.view.interfaces.ILoginView;
import com.google.android.gms.common.SignInButton;
import com.google.android.gms.common.api.GoogleApiClient;


/**
 * Handles login screen events
 */
public class LoginScreenView extends BasicView implements ILoginView {
    private static final String TAG = LoginScreenView.class.getName();
    private SignInButton signInButton;
    private Button noThanksButton;
    private LoginController loginController;

    /**
     * {@inheritDoc}
     */
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        return inflater.inflate(R.layout.login_screen_layout, container, false);
    }
    /**
     * {@inheritDoc}
     */
    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        GoogleApiClient googleApiClient = ((BaseActivity) getActivity()).getGoogleApiClient();
        GoogleApiCallbacksHandler googleApiCallbacksHandler = ((BaseActivity) getActivity()).getCallbacksHandler();
        signInButton = (SignInButton) view.findViewById(R.id.sign_in_button);
        noThanksButton = (Button) view.findViewById(R.id.no_thanks_button);
        loginController = new LoginController(this, googleApiClient, googleApiCallbacksHandler);
        loginController.setView(this);
        customizeButtons();
    }
    /**
     * {@inheritDoc}
     */
    @Override
    public void onResume() {
        super.onResume();
        Log.d(TAG, "registerLoginControllerCallback");
        loginController.registerCallbackHandler();

    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void onStop() {
        super.onStop();
        Log.d(TAG, "unregisterLoginControllerCallback");
        loginController.unregisterCallbackHandler();
    }

    private void customizeButtons() {
        signInButton.setColorScheme(SignInButton.COLOR_AUTO);
        signInButton.setOnClickListener(new View.OnClickListener() {
            /**
             *{@inheritDoc}
             */
            @Override
            public void onClick(View v) {
                loginController.googlePlusLogin();
            }
        });
        noThanksButton.setOnClickListener(new View.OnClickListener() {
            /**
             *{@inheritDoc}
             *
             */
            @Override
            public void onClick(View v) {
                loginController.runWithoutLogin(getActivity().getSupportFragmentManager());
            }
        });
    }
}
