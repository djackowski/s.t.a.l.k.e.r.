package com.blstream.stalker.view.fragments;

import android.graphics.Point;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.FragmentManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.StaggeredGridLayoutManager;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.blstream.stalker.BaseActivity;
import com.blstream.stalker.R;
import com.blstream.stalker.controller.CameraController;
import com.blstream.stalker.controller.DatabaseController;
import com.blstream.stalker.controller.GoogleApiCallbacksHandler;
import com.blstream.stalker.controller.GoogleDriveController;
import com.blstream.stalker.controller.PlaceListController;
import com.blstream.stalker.controller.database.DatabaseContract;
import com.blstream.stalker.controller.location.DetectActivityController;
import com.blstream.stalker.controller.location.LocationController;
import com.blstream.stalker.model.PlaceData;
import com.blstream.stalker.view.MyContentObserver;
import com.blstream.stalker.view.abstractClass.BasicView;
import com.blstream.stalker.view.adapters.PlaceListAdapter;
import com.blstream.stalker.view.interfaces.ContentObserverCallback;
import com.blstream.stalker.view.interfaces.IPlaceListView;
import com.google.android.gms.common.api.GoogleApiClient;

import java.util.ArrayList;
import java.util.List;

import static com.blstream.stalker.view.fragments.DetailItemView.NAME_BUNDLE_KEY;
import static com.blstream.stalker.view.fragments.DetailItemView.OPEN_HOURS_KEY;
import static com.blstream.stalker.view.fragments.DetailItemView.PLACE_DATA_KEY;
import static com.blstream.stalker.view.fragments.DetailItemView.TAGS_BUNDLE_KEY;

/**
 * Class defines list of place view.
 */
public class PlaceListView extends BasicView implements IPlaceListView, ContentObserverCallback {
    private static final String PLACE_DATA_LIST_KEY = "placeDataListKey";
    private DetectActivityController detectActivityController;
    private PlaceListAdapter adapter;
    private StaggeredGridLayoutManager mStaggeredLayoutManager;
    private MyContentObserver myContentObserver;
    private DatabaseController databaseController;
    private DetailItemView detailItemView;
    private GoogleApiClient googleApiClient;
    private CameraController cameraController;
    private FloatingActionButton buttonNewPhoto;
    private List<AsyncTask> asyncTaskArray;
    private GoogleDriveController googleDriveController;

    /**
     * {@inheritDoc}
     */
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        return inflater.inflate(R.layout.place_list_layout, container, false);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState){
        super.onViewCreated(view, savedInstanceState);
        adapter = new PlaceListAdapter(getContext());
        asyncTaskArray = new ArrayList<>();
        buttonNewPhoto = (FloatingActionButton) view.findViewById(R.id.floatingActionPhoto);
        initialControllers();
        initialRecyclerView(view);
        initialOnItemClickListener();
        initialScreenLayout();
        initialButtonBackListener();
        if (savedInstanceState != null) {
            ArrayList<PlaceData> placeDataArrayList = savedInstanceState.getParcelableArrayList(PLACE_DATA_LIST_KEY);
            adapter.setPlaceDataList(placeDataArrayList);
        } else {
            updateAfterDatabaseChanges();
        }
    }
    /**
     * {@inheritDoc}
     *
     */

    @Override
    public void onStart() {
        detectActivityController.onStart();
        cameraController.registerCallbackHandler();
        googleDriveController.registerCallbackHandler();
        super.onStart();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void onStop() {
        detectActivityController.onStop();
        super.onStop();
    }
    /**
     * {@inheritDoc}
     */
    @Override
    public void onResume() {
        super.onResume();
        detectActivityController.onResume();
        if (myContentObserver == null) {
            myContentObserver = new MyContentObserver(this);
        }
        getActivity().getContentResolver()
                .registerContentObserver(
                        DatabaseContract.URI_PLACES,
                        true,
                        myContentObserver);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void onDestroy() {
        cameraController.unregisterCallbackHandler();
        googleDriveController.unregisterCallbackHandler();
        super.onDestroy();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void onPause() {
        super.onPause();
        detectActivityController.onPause();
        getActivity().getContentResolver().unregisterContentObserver(myContentObserver);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putParcelableArrayList(PLACE_DATA_LIST_KEY, (ArrayList<PlaceData>) adapter.getPlaceDataList());
    }

    /**
     * Method seting span count to StaggeredGridLayoutManager as 2 if height is bigger than width,
     * 1 if width is bigger than height.
     */
    public void initialScreenLayout() {
        Point size = new Point();
        getActivity().getWindowManager().getDefaultDisplay().getSize(size);
        if (size.x > size.y) {
            mStaggeredLayoutManager.setSpanCount(2);
        } else {
            mStaggeredLayoutManager.setSpanCount(1);
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void updateList(List<PlaceData> placeDataList) {
        adapter.setPlaceDataList(placeDataList);
        adapter.notifyDataSetChanged();
    }

    /**
     * {@inheritDoc}
     * this is a callback method for a content observer
     * runs AsyncTask to get PlaceData list from database.
     */
    @Override
    public void updateAfterDatabaseChanges() {
        if (asyncTaskArray.isEmpty()) {
            asyncTaskArray.add(new AsyncTask<Object, Void, List<PlaceData>>() {
                @Override
                protected List<PlaceData> doInBackground(Object... params) {
                    return databaseController.getAllPlacesData();
                }

                @Override
                protected void onPostExecute(List<PlaceData> placeDatas) {
                    updateList(placeDatas);
                    asyncTaskArray.remove(this);
                }
            }.execute());
        }
    }

    /**
     * @return googleDriveController GoogleDriveController
     */
    public GoogleDriveController getGoogleDriveController() {
        return googleDriveController;
    }

    private void initialControllers() {
        databaseController = new DatabaseController(getContext());
        PlaceListController placeListController = new PlaceListController(this);
        placeListController.setView(this);
        detectActivityController = new DetectActivityController(this);
        googleApiClient = ((BaseActivity) getActivity()).getGoogleApiClient();
        GoogleApiCallbacksHandler googleApiCallbacksHandler = ((BaseActivity) getActivity()).getCallbacksHandler();
        cameraController = new CameraController(this, googleApiCallbacksHandler);
        cameraController.setView(this);
        googleDriveController = new GoogleDriveController(this, googleApiClient, googleApiCallbacksHandler);
    }

    private void initialButtonBackListener() {
        View currentView = getView();
        if (currentView != null) {
            currentView.setFocusableInTouchMode(true);
            currentView.requestFocus();
            View.OnKeyListener onKeyListener = new View.OnKeyListener() {
                @Override
                public boolean onKey(View v, int keyCode, KeyEvent event) {
                    if (keyCode == KeyEvent.KEYCODE_BACK) {
                        getActivity().finish();
                        return true;
                    }
                    return false;
                }
            };
            currentView.setOnKeyListener(onKeyListener);
        }
    }

    private void initialRecyclerView(View view) {
        RecyclerView recyclerView = (RecyclerView) view.findViewById(R.id.allTasks);
        mStaggeredLayoutManager = new StaggeredGridLayoutManager(1, StaggeredGridLayoutManager.VERTICAL);
        recyclerView.setLayoutManager(mStaggeredLayoutManager);
        recyclerView.setAdapter(adapter);
    }

    private void initialOnItemClickListener() {
        buttonNewPhoto.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (googleApiClient.isConnected()) {
                    cameraController.onCameraClick();
                } else {
                    if (!googleApiClient.isConnecting()) {
                        googleApiClient.connect();
                    }
                }
            }
        });
        PlaceListAdapter.OnItemClickListener onItemClickListener = new PlaceListAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(View v, int position) {
                detailItemView = new DetailItemView();
                createBundleForDetailsFragment(detailItemView, position);
                FragmentManager fragmentManager = getActivity().getSupportFragmentManager();
                fragmentManager.beginTransaction()
                        .replace(R.id.mainContainer, detailItemView).addToBackStack("")
                        .commit();
            }
        };
        adapter.setOnItemClickListener(onItemClickListener);
    }

    private void createBundleForDetailsFragment(DetailItemView detailItemView, int position) {
        List<PlaceData> placeDataList = adapter.getPlaceDataList();
        Bundle bundle = new Bundle();
        bundle.putString(NAME_BUNDLE_KEY, placeDataList.get(position).getName());
        bundle.putString(TAGS_BUNDLE_KEY, placeDataList.get(position).getTypes());
        bundle.putString(OPEN_HOURS_KEY, "11:00 - 22:00");
        bundle.putParcelable(PLACE_DATA_KEY, placeDataList.get(position));
        detailItemView.setArguments(bundle);
    }
}

