package com.blstream.stalker.view.abstractClass;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;


import com.blstream.stalker.R;

import com.blstream.stalker.view.fragments.ErrorMessageView;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import static org.junit.Assert.*;
import static org.mockito.Mockito.when;

public class BasicViewTest {

    public class ViewTest extends BasicView {
    }
    private ViewTest viewTest;
    @Mock
    private ConnectivityManager connectivityManager;
    @Mock
    NetworkInfo networkInfo;
    @Mock
    FragmentManager manager;
    @Mock
    Context context;
    @Mock
    FragmentTransaction fragmentTransaction;
    @Mock
    ErrorMessageView errorMessageView;
    int internetErrorId = R.string.no_internet_connection;
    int gpsErrorId = R.string.no_gps_connection;
    int internetAndGpsError = R.string.no_gps_and_internet_connection_error;
    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        viewTest = new ViewTest();
        when(manager.beginTransaction()).thenReturn(fragmentTransaction);
        when(fragmentTransaction.setCustomAnimations(R.anim.enter,R.anim.exit,R.anim.pop_enter,R.anim.pop_exit)).thenReturn(fragmentTransaction);
        when(fragmentTransaction.replace(R.id.mainErrorMessageContainer, BasicView.getErrorMessageInstance() )).thenReturn(fragmentTransaction);
    }
    @After
    public void tearDown(){

    }

    @Test
    public void shouldReturnFalseForInternetConnection() {
        //given
        when(connectivityManager.getActiveNetworkInfo()).thenReturn(networkInfo);
        when(networkInfo.isConnectedOrConnecting()).thenReturn(false);
        //when
        boolean isInternetConnection = viewTest.isInternetConnection(connectivityManager);
        //then
        assertFalse(isInternetConnection);
    }

    @Test
    public void shouldReturnTrueForInternetConnection() {
        //given
        when(connectivityManager.getActiveNetworkInfo()).thenReturn(networkInfo);
        when(networkInfo.isConnectedOrConnecting()).thenReturn(true);
        //when
        boolean isInternetConnection = viewTest.isInternetConnection(connectivityManager);
        //then
        assertTrue(isInternetConnection);
    }

}