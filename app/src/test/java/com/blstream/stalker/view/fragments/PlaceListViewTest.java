package com.blstream.stalker.view.fragments;

import android.content.Context;
import android.os.Bundle;
import android.support.v7.widget.StaggeredGridLayoutManager;
import android.view.View;

import com.blstream.stalker.Data;
import com.blstream.stalker.view.adapters.PlaceListAdapter;

import org.junit.Before;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class PlaceListViewTest extends Data {
    private PlaceListView placeListView;
    private PlaceListAdapter adapter;
    @Mock
    private Context context;
    @Mock
    private View view;
    @Mock
    private Bundle saveInstance;
    @Mock
    private StaggeredGridLayoutManager mStaggeredLayoutManager;

    @Before
    public void setUp() throws Exception {
        MockitoAnnotations.initMocks(this);
        mStaggeredLayoutManager = mock(StaggeredGridLayoutManager.class);
        placeListView = new PlaceListView();
        adapter = mock(PlaceListAdapter.class);
        placeListView.onViewCreated(view, saveInstance);
        when(view.getContext()).thenReturn(context);
        when(placeListView.getContext()).thenReturn(context);
    }
    //TODO tests will be implemented


}