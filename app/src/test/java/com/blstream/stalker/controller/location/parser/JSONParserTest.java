package com.blstream.stalker.controller.location.parser;

import com.blstream.stalker.model.OpenHours;
import com.blstream.stalker.model.Review;

import org.json.JSONObject;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

/**
 *
 */
public class JSONParserTest {

    private JSONParser jsonParser;
    private JSONObject object;

    @Before
    public void setUp() throws Exception {
        jsonParser = new JSONParser();
    }

    @After
    public void tearDown() throws Exception {
        object = null;
    }

    @Test
    public void testParseTypes() throws Exception {
        //given
        object = new JSONObject("{\"types\" : [ \"lodging\", \"point_of_interest\", \"establishment\" ]}");
        //when
        String actual = jsonParser.parseTypes(object);
        String expected = "\"lodging\", \"point of interest\", \"establishment\"";
        //then
        assertTrue(actual.equals(expected));
    }

    @Test
    public void testParseHours() throws Exception {
        //given
        object = new JSONObject("{\"opening_hours\" : {\n" +
                "         \"open_now\" : false,\n" +
                "         \"periods\" : [\n" +
                "            {\n" +
                "               \"close\" : {\n" +
                "                  \"day\" : 1,\n" +
                "                  \"time\" : \"1730\"\n" +
                "               },\n" +
                "               \"open\" : {\n" +
                "                  \"day\" : 1,\n" +
                "                  \"time\" : \"0830\"\n" +
                "               }\n" +
                "            },\n" +
                "            {\n" +
                "               \"close\" : {\n" +
                "                  \"day\" : 2,\n" +
                "                  \"time\" : \"1730\"\n" +
                "               },\n" +
                "               \"open\" : {\n" +
                "                  \"day\" : 2,\n" +
                "                  \"time\" : \"0830\"\n" +
                "               }\n" +
                "            },\n" +
                "            {\n" +
                "               \"close\" : {\n" +
                "                  \"day\" : 3,\n" +
                "                  \"time\" : \"1730\"\n" +
                "               },\n" +
                "               \"open\" : {\n" +
                "                  \"day\" : 3,\n" +
                "                  \"time\" : \"0830\"\n" +
                "               }\n" +
                "            },\n" +
                "            {\n" +
                "               \"close\" : {\n" +
                "                  \"day\" : 4,\n" +
                "                  \"time\" : \"1730\"\n" +
                "               },\n" +
                "               \"open\" : {\n" +
                "                  \"day\" : 4,\n" +
                "                  \"time\" : \"0830\"\n" +
                "               }\n" +
                "            },\n" +
                "            {\n" +
                "               \"close\" : {\n" +
                "                  \"day\" : 5,\n" +
                "                  \"time\" : \"1700\"\n" +
                "               },\n" +
                "               \"open\" : {\n" +
                "                  \"day\" : 5,\n" +
                "                  \"time\" : \"0830\"\n" +
                "               }\n" +
                "            }\n" +
                "         ],},}");
        //when
        OpenHours[] hours = jsonParser.parseEveryDayHours(object);
        String expected = "0830";
        String actual = hours[0].getOpenTime();
        //then
        assertTrue(actual.equals(expected));
    }

    @Test
    public void testParseIcon() throws Exception {
        //given
        object = new JSONObject("{\"photos\" : [\n" +
                "         {\n" +
                "            \"height\" : 2322,\n" +
                "            \"html_attributions\" : [\n" +
                "               \"\\u003ca href=\\\"https://www.google.com/maps/views/profile/107252953636064841537\\\"\\u003eWilliam Stewart\\u003c/a\\u003e\"\n" +
                "            ],\n" +
                "            \"photo_reference\" : \"CmRdAAAACsCA-RiBSfw3f9bV3oQeAkIPNLwBXEpdMOO99I-XMNQ45OxeYtiVyGDwW1paM6WyDmt4w-_qgegD6pUOMJp11urBAKrb84jDb7YoDxBFnlZuX1B1_P42pirZ1is2HeTyEhBCrNiBC2wO6M8ChUeyTxDIGhSdg9dqNDAQgodZjIAw9gIV_-2ROA\",\n" +
                "            \"width\" : 4128\n" +
                "         },],}");

        //when
        String actual = jsonParser.parseIconReference(object);
        String expected = "CmRdAAAACsCA-RiBSfw3f9bV3oQeAkIPNLwBXEpdMOO99I-XMNQ45OxeYtiVyGDwW1paM6WyDmt4w-_qgegD6pUOMJp11urBAKrb84jDb7YoDxBFnlZuX1B1_P42pirZ1is2HeTyEhBCrNiBC2wO6M8ChUeyTxDIGhSdg9dqNDAQgodZjIAw9gIV_-2ROA";
        //then
        assertTrue(actual.equals(expected));
    }

    @Test
    public void testParseRating() throws Exception {
        //given
        object = new JSONObject("{\"rating\" : 4.4}");

        //when
        double actual = jsonParser.parseRating(object);
        double expected = 4.4;
        double delta = 0.0;
        //then
        assertEquals(expected, actual, delta);
    }

    @Test
    public void testParseReviewList() throws Exception {
        //given
        object = new JSONObject("{\"reviews\" : [\n" +
                "         {\n" +
                "            \"aspects\" : [\n" +
                "               {\n" +
                "                  \"rating\" : 3,\n" +
                "                  \"type\" : \"overall\"\n" +
                "               }\n" +
                "            ],\n" +
                "            \"author_name\" : \"Danielle Lonnon\",\n" +
                "            \"author_url\" : \"https://plus.google.com/118257578392162991040\",\n" +
                "            \"language\" : \"en\",\n" +
                "            \"rating\" : 5,\n" +
                "            \"text\" : \"As someone who works in the theatre, I don't find the Google offices nerdy, I find it magical and theatrical. Themed rooms  with useful props and big sets with unique and charismatic characters. You sure this isn't a theatre company? Oh no wait Google has money, while the performing art does not.\",\n" +
                "            \"time\" : 1425790392\n" +
                "         },\n" +
                "         {\n" +
                "            \"aspects\" : [\n" +
                "               {\n" +
                "                  \"rating\" : 3,\n" +
                "                  \"type\" : \"overall\"\n" +
                "               }\n" +
                "            ],\n" +
                "            \"author_name\" : \"Rob Mulally\",\n" +
                "            \"author_url\" : \"https://plus.google.com/100839435712919930388\",\n" +
                "            \"language\" : \"en\",\n" +
                "            \"rating\" : 5,\n" +
                "            \"text\" : \"What can I say, what a great building, cool offices and friendly staff!\\nonly had a quick tour but there isn't much missing from this world class modern office.\\n\\nIf your staff who work here I hope you take advantage of all that it offers , because as a visitor it was a very impressive setup. \\n\\nThe thing that stood out besides the collaborative area's and beds for resting, was the food availability.\\n\\nImpressed. 5 Stars.\\n\",\n" +
                "            \"time\" : 1408284830\n" +
                "         },\n" +
                "         {\n" +
                "            \"aspects\" : [\n" +
                "               {\n" +
                "                  \"rating\" : 3,\n" +
                "                  \"type\" : \"overall\"\n" +
                "               }\n" +
                "            ],\n" +
                "            \"author_name\" : \"Michael Yeung\",\n" +
                "            \"author_url\" : \"https://plus.google.com/104161906493535874402\",\n" +
                "            \"language\" : \"en\",\n" +
                "            \"rating\" : 5,\n" +
                "            \"text\" : \"Best company in the world. The view from the cafeteria is unreal, you take in the entire Darling harbour view like nowhere else :)\",\n" +
                "            \"time\" : 1435313350\n" +
                "         },\n" +
                "         {\n" +
                "            \"aspects\" : [\n" +
                "               {\n" +
                "                  \"rating\" : 3,\n" +
                "                  \"type\" : \"overall\"\n" +
                "               }\n" +
                "            ],\n" +
                "            \"author_name\" : \"Ibrahim El-Jamal\",\n" +
                "            \"author_url\" : \"https://plus.google.com/103646390098458637797\",\n" +
                "            \"language\" : \"en\",\n" +
                "            \"rating\" : 5,\n" +
                "            \"text\" : \"Great track, great staff, overall great experience!!!!! \",\n" +
                "            \"time\" : 1434331674\n" +
                "         },\n" +
                "         {\n" +
                "            \"aspects\" : [\n" +
                "               {\n" +
                "                  \"rating\" : 3,\n" +
                "                  \"type\" : \"overall\"\n" +
                "               }\n" +
                "            ],\n" +
                "            \"author_name\" : \"Marco Palmero\",\n" +
                "            \"author_url\" : \"https://plus.google.com/103363668747424636403\",\n" +
                "            \"language\" : \"en\",\n" +
                "            \"rating\" : 5,\n" +
                "            \"text\" : \"I've been fortunate enough to have visited the Google offices on multiple occasions through the years and I've found this place to be quite awesome. This particular office is the original campus for Google Sydney and they've expanded to the Fairfax building where they've built an even more exciting office!\\n\\nTotally jealous of their cafeteria and the city views from their office!\",\n" +
                "            \"time\" : 1413529682\n" +
                "         }\n" +
                "      ],}");

        //when
        List<Review> actual = jsonParser.parseReviewList(object, 1);
        List<Review> expected = new ArrayList<>();
        expected.add(new Review("Danielle Lonnon",
                "As someone who works in the theatre, I don't find the Google offices nerdy, I find it magical and theatrical. Themed rooms  with useful props and big sets with unique and charismatic characters. You sure this isn't a theatre company? Oh no wait Google has money, while the performing art does not.",
                5));
        boolean compStatus = false;
        Review firstExpectedElementOfList = expected.get(0);
        Review firstActualElementOfList = actual.get(0);
        if (firstActualElementOfList.getAuthor().equals(firstExpectedElementOfList.getAuthor())
                && firstActualElementOfList.getRating() == firstExpectedElementOfList.getRating()
                && firstActualElementOfList.getReview().equals(firstExpectedElementOfList.getReview())) {
            compStatus = true;
        }

        //then
        assertTrue(compStatus);
    }
}