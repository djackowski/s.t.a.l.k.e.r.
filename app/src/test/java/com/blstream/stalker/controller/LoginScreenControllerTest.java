package com.blstream.stalker.controller;

import com.blstream.stalker.view.fragments.LoginScreenView;
import com.blstream.stalker.view.interfaces.IBasicView;
import com.google.android.gms.common.api.GoogleApiClient;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import static org.mockito.Mockito.verify;

public class LoginScreenControllerTest {

    LoginController loginScreenController;
    @Mock
    LoginScreenView loginScreenView;
    @Mock
    GoogleApiClient googleApiClient;
    @Mock
    GoogleApiCallbacksHandler googleApiCallbacksHandler;

    @Before
    public void setUp() throws Exception {
        MockitoAnnotations.initMocks(this);
        loginScreenController = new LoginController(loginScreenView, googleApiClient, googleApiCallbacksHandler);
        loginScreenController.setView(loginScreenView);
    }

    @After
    public void tearDown() throws Exception {
    }

    @Test
    public void runWithoutLogin() throws Exception {
        // given
        // when
        loginScreenController.runWithoutLogin();
        // then
        verify(loginScreenView).changeFragment(IBasicView.LIST_FRAGMENT);
    }
}