package com.blstream.stalker.broadcastReceiver;

import android.content.Context;
import android.content.Intent;

import com.blstream.stalker.controller.location.LocationService;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;

import static junit.framework.Assert.assertTrue;
import static org.mockito.Matchers.isA;
import static org.mockito.Mockito.verify;

/**
 * Tests ServiceBroadcastReceiver using Mockito
 */
public class ServiceBroadcastReceiverTest {

    Context context;
    ServiceBroadcastReceiverForTests receiver;
    Intent intent;

    @Before
    public void setUp() throws Exception {
        context = Mockito.mock(Context.class);
        receiver = new ServiceBroadcastReceiverForTests();
        intent = Mockito.mock(Intent.class);
    }

    @Test
    public void shouldCallUnregisterReciver() {
        //Given
        Mockito.doReturn(LocationService.ACTION_NOTIFICATION_DELETE).when(intent).getAction();

        //When
        receiver.onReceive(context, intent);
        //Then
        verify(context, Mockito.times(1)).unregisterReceiver(receiver);
    }

    @Test
    public void shouldCallStopService() {
        //Given
        Mockito.doReturn(LocationService.ACTION_NOTIFICATION_DELETE).when(intent).getAction();

        //When
        receiver.onReceive(context, intent);
        //Then
        verify(context, Mockito.times(1)).stopService(isA(Intent.class));
    }

    @Test
    public void shouldCallGetSystemService(){
        //Given
        Mockito.doReturn(LocationService.ACTION_APP_IN_BG).when(intent).getAction();
        //When
        //receiver.onReceive(context, intent);
        //Then
        //verify(context, Mockito.times(1)).getSystemService(Context.NOTIFICATION_SERVICE);
    }

}
