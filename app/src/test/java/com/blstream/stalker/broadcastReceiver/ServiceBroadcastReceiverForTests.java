package com.blstream.stalker.broadcastReceiver;

import android.content.Context;

import com.blstream.stalker.controller.location.ServiceBroadcastReceiver;

/**
 * Overrides sendNotification, to have access to it in tests.
 */
public class ServiceBroadcastReceiverForTests extends ServiceBroadcastReceiver {

    /**
     * Sends System notification with given message
     *
     * @param context app context
     * @param message to be set in notification
     */
    @Override
    protected void sendNotification(Context context, String message) {
        super.sendNotification(context, message);
    }
}
